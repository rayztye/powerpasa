<?php
include "logic.php";
checkRight(1);
include("crypt_class.php");
$crypt = new encryption();
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="pewekeyIcon.png">
    <link rel="stylesheet" href="index.css">
</head>
<body>
<div class="container">
    <h1>Benutzer bearbeiten</h1>
    <hr />
<?php

    if(isset($_POST["back"]))
        header("Location: index.php");

    if(isset($_POST["editUser"]))
    {

        echo "<div class='row'><div class='col-12'>";
        backButton("index.php", "", "Zurück");
        echo "</div></div>";

        // sql connection
        $con = mysqli_connect("", "root", "raspberry", "pewekey"); // db login
        $sql = "SELECT * FROM user"; // sql query
        $res = mysqli_query($con, $sql); // sql query in work
        $num = mysqli_num_rows($res); // read rows of db
        mysqliError($res);

        // start edit menu
        echo "<div class='container'>"; // generate a <div> with "container" - class
        if($_SESSION["status"] == 2) // check if right = true
        echo "<div class='row'>"; // generate a <div> with "row" - class
        echo "<div class='col'><b>Benutzername:</b></div>";
        echo "<div class='col'><b>Admin Status:</b></div>";
        echo "</div>"; // end "row" div

        // save sql data in variable
        while($dsatz = mysqli_fetch_assoc($res)) // read data from db
        {
            if($dsatz["username"] != $_SESSION["username"]) {
                if ($dsatz["admin"] == 0) $admin = "kein Admin";
                elseif ($dsatz["admin"] == 1) $admin = "Admin";
                else                          $admin = "super Admin";

                echo "<div class='row'>";
                if ($_SESSION["status"] == 2)
                    echo "<form method='post' action='editUser.php'>
                          <input type='hidden' name='user_id' value='" . $dsatz["user_id"] . "'/>
                          <input class='btn btn-info col' type='submit' value='Bearbeiten'></form>";
                echo "<div class='col'>" . $crypt->decrypt($dsatz['username']) . "</div>";
                echo "<div class='col'>" . $admin . "</div>";
                echo "</div><hr>";
            } // shows data from db
        }

    }
    // if "user_id" == true
    elseif(isset($_POST["user_id"])) // check if right = true
    {
        // is userstatus != 2 -> back
        if($_SESSION["status"] != 2)
        {
           header("Location: editUser.php");
           exit;
        }

        // sql connection
        $con = mysqli_connect("", "root", "raspberry", "pewekey");
        $sql = "SELECT * FROM user WHERE user_id = " . $_POST["user_id"];
        $res = mysqli_query($con, $sql);
        mysqliError($res);
        $dsatz = mysqli_fetch_assoc($res);

        // save sql admin data in variable
        if($dsatz["admin"] == 0)      $admin = "kein Admin";
        elseif ($dsatz["admin"] == 1) $admin = "Admin";
        else                          $admin = "Super Admins";


        // start table and save sql data in variable
        echo "<b>Bitte neuen Inhalt eintragen und speichern</b>
        <form action='editUser.php' method='post'>
        <div class='container'>
        <div class='row'>

            <div class='input-group'>
                <div class='input-group-prepend'>
                    <span class='input-group-text' id='basic-addon1'>Benutzername</span>
                </div>
                <input type='text' value='". $crypt->decrypt($dsatz["username"]) ."' name='username' class='form-control' placeholder='". $crypt->decrypt($dsatz["username"]) ."' aria-describedby='basic-addon1'>
            </div>
            <div class='input-group mb-3'>
                <div class='input-group-prepend'>
                    <span class='input-group-text' id='basic-addon1'>Admin Status: " .$admin."</span>
                </div>
                <select name='admin'>
                    <option value='0'>Kein Admin</option>
                    <option value='1'>Admin</option>
                    <option value='2'>Super Admin</option>
                </select>
            </div>
            <br><br>
            <div class='col-12'>
                <div class='btn-group' role='group' aria-label='Basic example'>
                    <form method='post' action='editUser.php'>
                        <input type='submit' class='btn btn-danger btn-secondary' name='editUser' value='zurück'>
                        <input type='hidden' name='newUserSend' value='" . $dsatz["user_id"] . "'>
                        <input class='btn-info btn btn-secondary' type='submit' value='Speichern'>
                    </form>
                </div> <!-- close btn-group -->
            </div> <!-- close col-12 -->
            </div> <!-- close row -->
        </div>"; // close container


    }
    elseif(isset($_POST["newUserSend"]))
    {
        echo "<form method='post'>
                <div class='btn-group' role='group' aria-label='Basic example'>
                <button type='submit' class='btn btn-danger btn-secondary' name='editUser'>Zurück</button>
                <button type='submit' class='btn btn-danger btn-secondary' name='back'>Zurück zum Start</button>
            </form>
            </div>";

        // save new sql data in variable
        $con = mysqli_connect("", "root", "raspberry", "pewekey");
        $username = $crypt->encrypt($_POST["username"]);
        $admin = $_POST["admin"];
        $id = $_POST["newUserSend"];

        // update sql database
        $sql = "UPDATE user SET username = '$username', admin = '$admin' WHERE user_id = '$id'";
        mysqli_query($con, $sql);

        success("Benutzer wurde bearbeitet");
    }
    echo "</div>";
    ?>
</div>
</body>
</html>
