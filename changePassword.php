<?php
  include "logic.php";
  include("crypt_class.php");
  $crypt = new encryption();
  ?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="pictures\pewekeypic.png">
    <link rel="stylesheet" href="index.css">
  </head>
  <body>
    <div class="container">
      <form method="post" action="changePassword.php">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Altes Passwort:</span>
          </div>
          <input name="oldPass" type="password" class="form-control" placeholder="Altes Passwort" aria-label="Password" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Neues Passwort:</span>
          </div>
          <input name="newPass1" type="password" class="form-control" placeholder="Neues Passwort" aria-label="Password" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Neues Passwort wiederholen:</span>
          </div>
          <input name="newPass2" type="password" class="form-control" placeholder="Neues Passwort wiederholen" aria-label="Password" aria-describedby="basic-addon1">
        </div>
          <div class="col">
          <button type="submit" class="btn btn-primary">Passwort ändern</button>
        </div>
      </form>
    </div>
  </body>
  <?php
  if(isset($_POST["oldPass"]))
  {

    $oldPass = sha1($_POST["oldPass"]);
    $newPass1 = $_POST["newPass1"];
    $newPass2 = $_POST["newPass2"];
    $con = mysqli_connect("","root", "raspberry", "pewekey");
    $sql = "SELECT password FROM user WHERE user_id = " . $_SESSION['userID'];
    $res = mysqli_query($con, $sql);
    $dsatz = mysqli_fetch_assoc($res);
    $sqlPass = implode("",$dsatz);

    if($sqlPass == $oldPass)
    {
      if($newPass1 == $newPass2)
      {
        $sql = "UPDATE user SET password = sha1($newPass1)";
        mysqli_query($con, $sql);
        echo "<div class='alert alert-success' role='alert'>
                Das Passwort wurde erfolgreich geändert!
              </div>";
      }
      else {
        echo "<div class='alert alert-danger' role='alert'>
                Die neuen Passwörter stimmen nicht überein!
              </div>";
      }
    }
    else{
      echo "<div class='alert alert-success' role='alert'>
              Die alten Passwörter stimmen nicht überein!
            </div>";
    }
  }

   ?>
</html>
