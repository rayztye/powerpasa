<?php
  include "logic.php";
  checkRight(2);
  include("crypt_class.php");
  $crypt = new encryption();
?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="pewekeyIcon.png">
    <link rel="stylesheet" href="index.css">
      <script type='text/javascript'>

          function selectUsers(minrole)
          {
              $('input[type="checkbox"][data-role]').each(function()
              {
                  var sel = $(this).data('role') >= minrole;
                  $(this).prop('checked',sel);
              });
          }
      </script>
  </head>
  <body>
  <div class="container"> <!-- Start container div and the Grid -->
      <h1>Daten löschen</h1>
      <form action="editData.php" class="form-inline" method="post">
          <input class="form-control mr-sm-2 col" name="search" type="text" placeholder="Suche" aria-label="Suche" required>
          <button class="btn btn-outline-primary my-2 my-sm-0 col" type="submit">Suche</button>
      </form>
      <hr />
<?php

  if(isset($_POST["deleteData"]) || isset($_POST["search"]))
  {
    echo "<div class='row'>";

    echo "<div class='col'>";
    backButton("index.php", "", "Zurück");
    echo "</div>";

    // sql connection
    $userID = $_SESSION['userID'];

    $con = mysqli_connect("", "root", "raspberry", "pewekey");
    $sql = "select data.* from data
    left join user_data on user_data.data_id=data.data_id
    where user_data.user_id = " . $userID;
    $res = mysqli_query($con, $sql);
      mysqliError($res);

    // save SQL-data in variable
    while($dsatz = mysqli_fetch_assoc($res))
    {
        $creationDate = date("H:i:s - d.m.Y", $dsatz["createdDate"]);
        $lastChange = date("H:i:s - d.m.Y", $dsatz["lastChange"]);
?>
        <div class='col-12'>Benutzername: <?=$crypt->decrypt($dsatz["user"]) ?></div>
        <div class='col-12'>Passwort: <?=$crypt->decrypt($dsatz["password"]) ?></div>
        <div class='col-12'>Notiz: <?=$crypt->decrypt($dsatz["note"]) ?></div>
        <div class='col-12'>URL: <?=$crypt->decrypt($dsatz["url"]) ?></div>
        <div class='col-12'>Zuletzt bearbeitet von: <?=$crypt->decrypt($dsatz["user_id"]) ?></div>
        <div class='col-12'>Ertsellt am: <?=$creationDate ?></div>
        <div class='col-12'>Zuletzt bearbeitet am: <?= $lastChange ?></div>
        <div class='col-12'><form method='post' action='deleteData.php'><input type='hidden' name='data_id' value='<?=$dsatz["data_id"]?>'/><input class='btn btn-danger' type='submit' value='Entfernen'></form></div><hr />
<?php
    }
  echo "</div>";
  }
  elseif(isset($_POST["data_id"]))
  {
    $con = mysqli_connect("", "root", "raspberry", "pewekey");
    $sql = "DELETE FROM data
    WHERE data_id = " . $_POST["data_id"];
    mysqli_query($con, $sql);
    $sql1 = "DELETE FROM user_data
    WHERE data_id = " . $_POST["data_id"];
    mysqli_query($con, $sql1);


    success("Daten wurden gelöscht");
    backButton("deleteData.php", "deleteData", "Zurück");
    backButton("index.php", "", "Zurück zum Start");
  }
?>
    </div>
  </body>
</html>
