
<?php
session_start();

if(!isset($_SESSION["authorized_user"]) || $_SESSION["authorized_user"] != 1 )
{
    header("Location: login.php");
    exit;
}

if( !isset($_SESSION["status"]) )
    $_SESSION["status"] = 0;

#################
### functions ###
#################

// error message
function error($error)
{
    echo "<div id='error'><b>Fehler: $error</b><br></div>";
}

// success message
function success($success)
{
    echo "<div id='success'><b>$success</b><br></div>";
}

// back button
function backButton($action, $name, $value)
{
    echo "<form action='$action' method='post'>";
    echo "<input class='btn btn-danger' type='submit' name='$name' value='$value'>";
    echo "</form>";
}

function checkRight($minRight)
{
    if( $_SESSION["status"] < $minRight )
    {
        header("Location: index.php");
        die();
    }
}

function cleanHTML($clean="")
{
    $clean = trim($clean);
    $clean = htmlentities($clean, ENT_QUOTES, "UTF-8");
    return $clean;
}

function mysqliError($data)
{
    if($data === false)
    {
        echo "<div class='alert alert-danger' role='alert'>Keine Daten vorhanden</div>";
        exit();
    }
}
###################
### HTML - code ###
###################
?>
<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="/index.css">
  <link rel="icon" type="image/png" href="pictures\pewekeypic.png">
  <title>PowerPasa</title>
</head>
<body>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <script type='text/javascript' src='check.js'></script>
  <nav class="navbar navbar-expand-lg bg-primary navbar-dark">
    <a class="navbar-brand" href="index.php">Start</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="nav navbar-nav">
        <?php if( $_SESSION["status"] >= 0 ): ?>
          <li class="nav-item active">
            <form action="listData.php" method="post">
              <input class="btn btn-primary" type="submit" name="listData" value="Zugangsdaten anzeigen/ bearbeiten">
            </form>
          </li>
        <?php endif; ?>
        <li class="nav-item">
          <?php if( $_SESSION["status"] >= 1 ): ?>
            <li class="nav-item">
              <form action="newData.php" method="post">
                <input class="btn btn-primary" type="submit" name="newData" value="Neue Zugangsdaten eintragen">
              </form>
            </li>
            <li class="nav-item active">
              <form action="editUser.php" method="post">
                <input class="btn btn-primary" type="submit" name="editUser" value="Benutzer anzeigen/ bearbeiten">
              </form>
            </li>
            <?php endif; ?>
            <?php if( $_SESSION["status"] >= 2 ): ?>
              <li>
                <form action="deleteData.php" method="post">
                  <input class="btn btn-primary" type="submit" name="deleteData" value="Zugangsdaten Löschen">
                </form>
              </li>
            <li class="nav-item">
              <form action="newUser.php" method="post">
                <input class="btn btn-primary" type="submit" name="newUser" value="Neuen Benutzer anlegen">
              </form>
            </li>
            <li class="nav-item">
              <form action="deleteUser.php" method="post">
                <input class="btn btn-primary" type="submit" name="deleteUser" value="Benutzer Löschen">
              </form>
            <?php endif; ?>
          </li>
          <?php if( $_SESSION["status"] >= 0 ): ?>
            <li class="nav-item active">
              <form action="changePassword.php" method="post">
                <input class="btn btn-primary" type="submit" name="change" value="Passwort ändern">
              </form>
            </li>
          <?php endif; ?>
          <li class="nav-item">
            <form action="ini.php" method="post">
              <span class="glyphicon glyphicon-log-in"><input class="btn btn-danger" type="submit" name="logout" value="Ausloggen"></span>
            </form>
          </li>
        </ul>
      </div>
    </nav>
  </body>
</html>
