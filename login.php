  <?php
  include "ini.php";
?>
<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="pewekeyicon.png" size="96x96">
  <link rel="stylesheet" href="index.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <div id="login" class="container">
    <form action="ini.php" method="post">
      <div class="row">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Username:</span>
          </div>
          <input class="form-control" name="username" required>
        </div>
      </div>
      <div class="row">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Password:</span>
          </div>
          <input class="form-control" name="password" type="password" required>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-4" id="login">
          <button type="submit" class="btn btn-primary btn-lg btn-block" name="send">Login</button>
        </div>
      </div>
    </form>
  </div>
</body>
</html>
