<?php
  include "logic.php";
  checkRight(2);
  include("crypt_class.php");
  $crypt = new encryption();
?>
<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="pewekeyIcon.png">
  <link rel="stylesheet" href="index.css">
</head>
<body>
  <div class="container">
    <h1>Benutzer löschen</h1>
    <hr />
<?php
    if(isset($_POST["deleteUser"]))
    {
      echo "<div class='row'>";
      echo "<div class='col-12'>";
      backButton("index.php", "", "Zurück");
      echo "</div><br /><br />";
      // sql
      $con = mysqli_connect("", "root", "raspberry", "pewekey");
      $sql = "SELECT * FROM user";
      $res = mysqli_query($con, $sql);
        mysqliError($res);

      // save SQL-data in variable
      while($dsatz = mysqli_fetch_assoc($res))
      {
        if($dsatz["username"] != $_SESSION["username"])
        {
          if ($dsatz["admin"] == 0) $admin = "keine Admin Rechte";
          elseif ($dsatz["admin"] == 1) $admin = "mittlere Rechte";
          else                          $admin = "super Admin";

              echo "<div class='col-12'>Benutzername: "  . $crypt->decrypt($dsatz["username"]) . "</div>
                    <div class='col-12'> Adminstatus: " . $admin . "</div>
                    <div class='col-12'><form method='post' action='deleteUser.php'><input type='hidden' name='user_id' value='" . $dsatz["user_id"] . "'><input class='btn btn-danger' type='submit' value='Entfernen'></form></div><hr />";
        }
      }
      echo "</div></div>";
    }
    elseif(isset($_POST["user_id"]))
    {
      // save new data in sql database
      $con = mysqli_connect("", "root", "raspberry", "pewekey");
      $sql = "DELETE FROM user
      WHERE user_id = " . $_POST["user_id"];
      mysqli_query($con, $sql);

      success("Benutzer wurde gelöscht");
      backButton("deleteUser.php", "deleteUser", "Zurück");
      backButton("index.php", "", "Zurück zum Start");
    }
?>
  </div>
</body>
</html>
