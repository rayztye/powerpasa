<?php
  // session start
  session_start();

  include("crypt_class.php");
  $crypt = new encryption();

  #################
  ### functions ###
  #################

  // error message
  function error($error)
  {
    echo "<div id='error'><b>Fehler: $error</b><br></div>";
  }

  // success message
  function success($success)
  {
    echo "<div id='success'><b>$success</b><br></div>";
  }

  ############
  ### code ###
  ############

  // if send set?
  if(isset($_POST["send"])) {

    // if isset username or password
    if (!isset($_POST["username"]) || !isset($_POST["password"])) {
      header("Location: login.php");
      exit;
    }

    // save formdata in variable
    $useform = $crypt->encrypt($_POST["username"]);
    $pasform = $_POST["password"];
    if (empty($useform) || empty($pasform)) {
      header("Location: login.php");
      exit;
    }

    // take database connection up
    $con = mysqli_connect("", "root", "raspberry", "pewekey");
    // sql statement running
    $sql = "SELECT *
        FROM user
        WHERE username = '$useform' AND password = sha1('$pasform')";

    // SQL query save in variable
    $res = mysqli_query($con, $sql);

    // checking connection
    if ($res != false) {
    $usesql = false;
    $passql = false;

    // save MySQL data in variable
    while ($dsatz = mysqli_fetch_assoc($res)) {
      $usesql = $crypt->decrypt($dsatz["username"]);
      $passql = $dsatz["password"];
      $status = $dsatz["admin"];
      break;
    }

    // checking login
    if ($usesql && $passql) {
      // if login == true => code here:
      $_SESSION["authorized_user"] = 1;
      $_SESSION["username"] = $useform;
      $_SESSION["status"] = $status;

      // Save user_id from database in a session
        $con = mysqli_connect("", "root", "raspberry", "pewekey");
        $sql = "SELECT user_id FROM user WHERE username= '". $_SESSION["username"]."'";
        $res = mysqli_query($con, $sql);
        $num = mysqli_fetch_assoc($res);
        $_SESSION["userID"] = $num['user_id'];
        $userID = $_SESSION['userID'];
      header("Location: index.php");
    } else {
      // if login == false
      header("Location: login.php");
    }
    } else {
      error("Verbindung");
    }
  }

  // logout
  if(isset($_POST["logout"])){
    $con = mysqli_connect("", "root", "raspberry", "pewekey");
    $sql = "UPDATE user SET lastLogin = '".time()."'WHERE username = '".$_SESSION["username"]."'";
    mysqli_query($con, $sql);
    mysqli_close($con);
    session_destroy();
    header("Location: login.php");
  }
