<?php
include "logic.php";
?>
<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<?php
include("crypt_class.php");
$c = new encode();
$c->readLine();

if(isset($_POST["encodeText"])) {
    $encodeText = $_POST["encodeText"];
    $c->symmetric($encodeText);
}
