<?php
  include "logic.php";
  include("crypt_class.php");
  $crypt = new encryption();
  ?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="pictures\pewekeypic.png">
    <link rel="stylesheet" href="index.css">
  </head>
  <body>
    <div class="container">
      <div class="container-fluid">
        <div class="d-flex align-text-center">
          <div class="mx-auto">
            <?php $user = $crypt->decrypt($_SESSION["username"]);?>
              <h2><span id='welcome'>Willkommen, <?=$user?></span></h2>
              <div>Es ist <?=date("H:i", time())?> Uhr, und wir haben den <?=date("d.m.Y", time())?>. &nbsp;</div>
                <?php
                  $con = mysqli_connect("", "root", "raspberry", "pewekey");
                  $username = $_SESSION["username"];
                  $sql = "SELECT lastLogin FROM user WHERE username = '$username'";
                  $res = mysqli_query($con, $sql);
                  $dsatz = mysqli_fetch_assoc($res);
                ?>
            <p> Ihr letzter Login war am: <?=date('d.m.Y', $dsatz["lastLogin"])?> um <?=date('H:i', $dsatz["lastLogin"])?> Uhr.</p>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
