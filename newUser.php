<?php
  include "logic.php";
  checkRight(2);
  include("crypt_class.php");
  $crypt = new encryption();
?>
<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="pewekeyIcon.png">
  <link rel="stylesheet" href="index.css">
</head>
<body>
  <div class="container">
    <h1>Neuen Benutzer erstellen</h1>
    <hr />
<?php
    // insert new User in Database
    if(isset($_POST["newUser"]))
    {
      ?>
      <form method='post' action='newUser.php'><div class='row'>

        <div class='col'>
          <?php backButton("index.php", "", "Zurück") ?>
        </div>

        <div class='input-group'>
          <div class='input-group-prepend'>
            <span class='input-group-text' id='inputGroup-sizing-gefault'>Benutzername</span>
          </div>
          <input class='form-control' name='user' type='text' required>
        </div>

        <div class='input-group'>
          <div class='input-group-prepend'>
            <span class='input-group-text' id='inputGroup-sizing-gefault'>Passwort</span>
          </div>
          <input class='form-control' name='password' type='text' required>
        </div>

        <div class='input-group'>
          <div class='input-group-prepend'>
            <span class='input-group-text' id='inputGroup-sizing-gefault'>Adminstatus</span>
          </div>
          <select class='form-control' name='admin'>
            <option value='0'>keine Rechte</option>
            <option value='1'>mittlere Rechte</option>
            <option value='2'>alle Rechte</option></select>
          </div>
          <div class='col'>
            <input class='btn btn-info  ' name='newUserForm' type='submit' value='neu Eintragen' required>
          </div>
        </div> <!-- close row -->

      </form>
<?php
    }
    if(isset($_POST["newUserForm"]))
    {
      // save data in variable

      $user = $crypt->encrypt($_POST["user"]);
      if(!isset($_POST["admin"]))
      $admin = "0";
      else
      $admin = $_POST["admin"];
      $password = $_POST["password"];

      // save new data in sql database
      $con = mysqli_connect("", "root", "raspberry", "pewekey");
      $sql = "INSERT INTO user SET username = '$user', password = sha1('$password'), admin='$admin' ";
      mysqli_query($con, $sql);

      success("Neuer benutzer wurde angelegt");
      backButton("newUser.php", "newUser", "Zurück");
      backButton("index.php", "", "Zurück zum Start");
    }
?>
    </div>
  </div>
</body>
</html>
