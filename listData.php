<?php
include "logic.php";
checkRight(0);
include("crypt_class.php");
$crypt = new encryption();
?>
<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="pewekeyIcon.png">
<link rel="stylesheet" href="index.css">
  <script type='text/javascript'>

  function selectUsers(minrole)
  {
    $('input[type="checkbox"][data-role]').each(function()
    {
      var sel = $(this).data('role') >= minrole;
      $(this).prop('checked',sel);
    });
  }
</script>
</head>
<body>
<div class="container"> <!-- Start container div and the Grid -->

  <h1>Daten bearbeiten</h1>
  <form action="listData.php" class="form-inline" method="post">
    <input class="form-control mr-sm-2 col" name="search" type="text" placeholder="Suche" aria-label="Suche" required>
    <button class="btn btn-outline-primary my-2 my-sm-0 col" type="submit">Suche</button>
  </form>
  <hr />
  <div class='row'> <!-- start row div -->
<?php

if(isset($_POST["listData"]) || isset($_POST["search"]))
{
  $userID = $_SESSION['userID'];

  // sql connection
  $con = mysqli_connect("", "root", "raspberry", "pewekey");
  $sql = "select data.* from data
  left join user_data on user_data.data_id=data.data_id
  where user_data.user_id = " . $userID;
  $res = mysqli_query($con, $sql);

  mysqliError($res);

  echo "<div class='col-12'>";
  backButton("index.php", "", "Zurück");
  echo "</div>";
  echo "<br>";


  // save sql data in variable
  while($dsatz = mysqli_fetch_assoc($res))
  {
    foreach( ['user','password','note','url','user_id'] as $k )
      $dsatz[$k] = $crypt->decrypt($dsatz[$k]); // descrypt data

    if(isset($_POST["search"]) && $_POST["search"] ) // check if search not null
    {
      if( stripos($dsatz['user'],$_POST["search"]) === false ) // search $search in array
        continue;
    }

    $creationDate = date("d.m.Y - H:i:s", $dsatz["createdDate"]);
    $lastChange = date("d.m.Y - H:i:s", $dsatz["lastChange"]);
?>

    <div class='col-12'> URL: <b><?=$dsatz['url']?></b> <a href="<?=$dsatz['url']?>" title="gehe zu <?=$dsatz['url']?>" target= "_blank">&#8599;</a></div>
    <div class='col-12'> Benutzername: <u><?=$dsatz['user']?></u>, Passwort: <u><?=$dsatz['password']?></u></div>
    <div class='col-12'> Zuletzt bearbeitet von: <?=$dsatz['user_id']?>.
<?php
    if($_SESSION["status"] >= 1)
    {
      echo " Erstellt am: " . $creationDate;
      echo ". Zuletzt bearbeitet am: " . $lastChange . "</div>";
    }
    if(isset($dsatz['note']) && $dsatz['note'])
      echo "<div class='col-12'> Notiz: " . $dsatz['note'] . "</div>";

    if($_SESSION["status"] >= 1)
      echo "<div class='col-12'><form method='post' action='editData.php'><input type='hidden' name='editData' value='".$dsatz["data_id"]."'/><input class='btn btn-lg btn-info btn-primary' type='submit' value='Bearbeiten'></form></div>";
    echo "<hr />"; // close row div
  } // end while
}
