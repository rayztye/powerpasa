<?php
  include "logic.php";
  checkRight(1);
  include("crypt_class.php");
  $crypt = new encryption();
?>
<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="pewekeyIcon.png">
  <link rel="stylesheet" href="index.css">
  <script type='text/javascript'>
    function selectUsers(minrole)
    {
      $('input[type="checkbox"][data-role]:not([disabled])').each(function()
      {
        var sel = $(this).data('role') >= minrole;
        $(this).prop('checked',sel);
      });
    }
  </script>
</head>
<body>
  <div class="container">
    <h1>Neue Daten eintragen</h1>
    <hr />
<?php
    // insert new data in the database
    if(isset($_POST["newData"]))
    {
      $con = mysqli_connect("","root", "raspberry", "pewekey");
      $sql = "SELECT * FROM user";
      $res = mysqli_query($con, $sql);
        mysqliError($res);
?>
      <!--<div class='row'>--><div class='col-12'>
      <?php backButton("index.php", "", "Zurück"); ?>
    </div>
      <form action='newData.php' method='post'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='inputGroup-sizing-gefault'>Benutzername</span>
            </div>
            <input class='form-control' name='user' type='text' required>
          </div>

          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='inputGroup-sizing-gefault'>Passwort</span>
            </div>
            <input class='form-control' name='password' type='text' required>
          </div>

          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='inputGroup-sizing-gefault'>Notizen</span>
            </div>
            <input class='form-control' name='note' type='text'>
          </div>

          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='inputGroup-sizing-gefault'>URL</span>
            </div>
            <input class='form-control' name='url' type='text' required>
          </div>

          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='inputGroup-sizing-gefault'>Sichtbar für:</span>
            </div>
            <div class="btn-group" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-primary" onclick='selectUsers(0)'>Für Jeden</button>
              <button type="button" class="btn btn-primary" onclick='selectUsers(1)'>Für Admins</button>
              <button type="button" class="btn btn-primary" onclick='selectUsers(2)'>Für Superadmin</button>
              <button type="button" class="btn btn-primary" onclick='selectUsers(10)'>Für Mich</button>
            </div>

          </div> <!-- close input-group div -->
          <div class="col">
            <input type='hidden' name='user_ids[]' value='" . <?=$_SESSION["userID"]?> . "'>
<?php
          $con = mysqli_connect("", "root", "raspberry", "pewekey");
          $sql = "SELECT * FROM user";
          $res = mysqli_query($con, $sql);
          $i = 0;
          while($dsatz = mysqli_fetch_assoc($res))
          {
?>
            <div class='form-check'>
            <input name='user_ids[]' value='<?=$dsatz["user_id"]?>' class='form-check-input' type='checkbox' id='blankCheckbox<?=$i?>' data-role='<?=$dsatz["admin"]?>' <?=($dsatz["user_id"] == $_SESSION["userID"]) ? 'checked="checked" disabled="disabled"':'' ?>/>
            <input name='user_ids[]' value="<?=$_SESSION["userID"]?>" type="hidden">
            <label class='form-check-label' for='blankCheckbox<?=$i?>'>
            <?php echo $crypt->decrypt($dsatz["username"]) ?>
            </label>
            </div>
<?php
            $i++;
          }
?>
          </div> <!-- close row div -->
          <div class='input-group'><input class='btn btn-info' name='newDataForm' type='submit' value='neu Eintragen' required></div>
        </div> <!-- close row div -->
      </form>
<?php }
    elseif(isset($_POST["newDataForm"]))
    {
      $con = mysqli_connect("", "root", "raspberry", "pewekey");

      // data save in variable
      $newDate =      time();
      $user =         $crypt->encrypt($_POST["user"]);
      $password =     $crypt->encrypt($_POST["password"]);
      $note =         $crypt->encrypt($_POST["note"]);
      $url =          $crypt->encrypt($_POST["url"]);
      $user_id =      ($_SESSION["username"]);
      if(!isset($_POST["admin"]))
        $crypt->encrypt($admin = "0");
      else
        $crypt->encrypt($admin = $_POST["admin"]);

      // save new sql data in database
      $sql = "INSERT INTO data SET createdDate = '$newDate', lastChange = '$newDate', user = '$user', password = '$password', note = '$note', url = '$url', user_id='$user_id'";
      mysqli_query($con, $sql);
      $dataID = mysqli_insert_id($con);

      // save user_ids in user_data
      mysqli_query($con, "DELETE FROM user_data WHERE data_id='$dataID'");
      $user_ids = $_POST['user_ids'];
      foreach( $user_ids as $userID )
      {
        $sql2 = "INSERT INTO user_data(user_id,data_id)VALUES('$userID','$dataID')";
        mysqli_query($con, $sql2);
      }

      success("Neuer Datensatz wurde hinzugefügt");
      backButton("newData.php", "newData", "Zurück");
      backButton("index.php", "", "Zurück zum Start");
    }
?>
  </div> <!-- close container div -->
</body>
</html>
