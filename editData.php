<?php
  include "logic.php";
  checkRight(0);
  include("crypt_class.php");
  $crypt = new encryption();
?>
<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="pewekeyIcon.png">
  <link rel="stylesheet" href="index.css">
    <script type='text/javascript'>
    function selectUsers(minrole)
    {
      $('input[type="checkbox"][data-role]:not([disabled])').each(function()
      {
        var sel = $(this).data('role') >= minrole;
        $(this).prop('checked',sel);
      });
    }
  </script>
</head>
<body>
  <div class="container"> <!-- Start container div and the Grid -->
    <h1>Daten bearbeiten</h1>
    <hr />
  <?php

    // if set data_id ?
    if(isset($_POST["editData"]))
    {

      // sql connection
      $con = mysqli_connect("", "root", "raspberry", "pewekey");
      $sql = "SELECT * FROM data WHERE data_id = " . $_POST["editData"];
      $res = mysqli_query($con, $sql);
      $dsatz = mysqli_fetch_assoc($res);
        mysqliError($res);

      // gridsystem start and save sql data in variable
  ?>
      <form action='editData.php' name='newDataSend' method='post'>
        <div class="row">
        <!-- URL -->
        <div class='input-group'>
          <div class='input-group-prepend'>
            <span class='input-group-text' id='inputGroup-sizing-gefault'>URL</span>
          </div>
          <input class='form-control' name='url' value='<?=$crypt->decrypt($dsatz["url"])?> '>
        </div> <!-- end of input-group -->

      <!-- USERNAME -->
      <div class='input-group'> <!-- start input-group -->
        <div class='input-group-prepend'>  <!-- start input-group-prepend -->
          <span class='input-group-text' id='inputGroup-sizing-gefault'>Benutzername</span> <!--input-tag -->
        </div> <!-- end of input-group-prepend -->
        <input class='form-control' name='user' value='<?=$crypt->decrypt($dsatz["user"]) ?>'> <!-- input -->
      </div> <!-- end of input-group -->

      <!-- PASSWORD -->
      <div class='input-group'> <!-- start input-group -->
        <div class='input-group-prepend'> <!-- start input-group-prepen -->
          <span class='input-group-text' id='inputGroup-sizing-gefault'>Passwort</span> <!-- input-tag -->
        </div> <!-- end of input-group-prepend -->
        <input class='form-control' name='password' value='<?=$crypt->decrypt($dsatz["password"]) ?>'> <!-- input -->
      </div> <!-- end of input-group -->

      <!-- NOTICE -->
      <div class='input-group'> <!-- start input-group -->
        <div class='input-group-prepend'> <!-- start input-group-prepend -->
          <span class='input-group-text' id='inputGroup-sizing-gefault'>Notiz</span> <!-- input-tag -->
        </div> <!-- end of input-group-prepend -->
        <input class='form-control' name='note' value='<?=$crypt->decrypt($dsatz["note"]) ?>'> <!-- input -->
      </div> <!-- end of input-group -->

      <!-- Who can view the Data? -->
      <div class='input-group'>
        <div class='input-group-prepend'>
          <span class='input-group-text' id='inputGroup-sizing-gefault'>Sichtbar für:</span>
        </div>
        <div class="btn-group" role="group" aria-label="Basic example">
          <button type="button" class="btn btn-primary" onclick='selectUsers(0)'>Für Jeden</button>
          <button type="button" class="btn btn-primary" onclick='selectUsers(1)'>Für Admins</button>
          <button type="button" class="btn btn-primary" onclick='selectUsers(2)'>Für Superadmin</button>
          <button type="button" class="btn btn-primary" onclick='selectUsers(10)'>Niemand</button>
        </div>
      </div> <!-- close input-group div -->
    </div>
      <div class="col">
  <?php
      $con = mysqli_connect("", "root", "raspberry", "pewekey");
      $sql = "SELECT user.*, user_data.data_id FROM user left join user_data on user_data.user_id=user.user_id and user_data.data_id=".$_POST["editData"];
      $res = mysqli_query($con, $sql);
      $i = 0;
  mysqliError($res);

      while($dsatz = mysqli_fetch_assoc($res))
      {
  ?>
      <div class='form-check'>
        <input name='user_ids[]' value='<?=$dsatz["user_id"]?>' class='form-check-input' type='checkbox' id='blankCheckbox<?=$i?>' data-role='<?=$dsatz["admin"]?>' <?=($dsatz["user_id"] == $_SESSION["userID"]) ? 'checked="checked" disabled="disabled"':''?> <?=$dsatz['data_id']?'checked="checked"':''?>>
          <label class='form-check-label' for='blankCheckbox<?=$i?>'>
            <?php echo $crypt->decrypt($dsatz["username"]) ?>
        </label>
      </div>
  <?php
      $i++;
      }
  ?>
      </div> <!--  end of input-group -->
      <div class="row">
        <div class="col">
          <!-- start btn-group div -->
              <input type='hidden' name='data_id' value='<?=$_POST["editData"] ?>'>
              <input type="hidden" name="save" value="1">
            <button class='btn btn-info btn-secondary' type='submit'>Speichern</button>
            </form>
            <form action="listData.php" method="post">
            <button type='submit' class='btn btn-danger btn-secondary' name='listData'>Zurück</button>
          </form>
        </div> <!-- close col div -->
      </div> <!-- close row div -->
  <?php
    }
    // if "data_id" == true
    elseif(isset($_POST["save"]))
    {
  ?>
    <div class='btn-group' role='group' aria-label='Basic example'>
      <form method='post' action="listData.php">
        <button type='submit' class='btn btn-danger btn-secondary' name='listData'>Zurück</button>
      </form>
      <form method='post' action='index.php'>
        <button type='submit' class='btn btn-danger btn-secondary'>Zurück zum Start</button>
      </form>
    </div><br>
    <?php
    $crypt = new encryption();

    // new sql data
    $con = mysqli_connect("", "root", "raspberry", "pewekey");
    $user       = $crypt->encrypt($_POST["user"]);
    $password   = $crypt->encrypt($_POST["password"]);
    $note       = $crypt->encrypt($_POST["note"]);
    $url        = $crypt->encrypt($_POST["url"]);
    $id         = $_POST["data_id"];
    $lastChange = time();

    $dataID = $_POST["data_id"];
    // save new sql data in database
    $sql = "UPDATE data SET user = '$user', password = '$password', note = '$note', url = '$url', lastChange = '$lastChange' WHERE data_id = '$id' ";
    mysqli_query($con, $sql);

    // save Adminright of data
    mysqli_query($con, "DELETE FROM user_data WHERE data_id='$dataID'");

    $user_ids = isset($_POST['user_ids'])?$_POST['user_ids']:[];
    $user_ids[] = $_SESSION['userID'];
    foreach( $user_ids as $userID )
    {
      $sql2 = "INSERT IGNORE INTO user_data(user_id,data_id)VALUES('$userID','$dataID')";
      mysqli_query($con, $sql2);
    }
    success("Daten wurden geändert");
    }
  echo "</div>"; // close container div
  ?>
  </div>
</body>
</html>
